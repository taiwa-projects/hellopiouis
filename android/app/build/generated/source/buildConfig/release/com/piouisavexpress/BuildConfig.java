/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.piouisavexpress;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.piouisavexpress";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 5;
  public static final String VERSION_NAME = "1.5";
}
