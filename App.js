import React, { createContext, useState,useEffect, useContext, useCallback } from 'react';
import {
  PermissionsAndroid,
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Linking,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  Alert
} from 'react-native';
import { CheckBox } from 'react-native-elements'
import Voice from '@react-native-voice/voice';
import { GiftedChat } from 'react-native-gifted-chat'
import { Button, GooglePlayButton } from "@freakycoder/react-native-button";
import Tts from 'react-native-tts';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Reinput from 'reinput'
import auth from '@react-native-firebase/auth';
import axios from 'axios';
import Toast from 'react-native-toast-message';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Icon from 'react-native-vector-icons/Ionicons';
import { Menu, MenuItem, MenuDivider } from 'react-native-material-menu';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';


const url = 'https://us-central1-pioui-48a5c.cloudfunctions.net/api/'

const App  =  () =>

{
  let lastIntent = ''
  let currentUserRef = null
  
  /**
   * Homescreen View
   * @returns 
   */
   const HomeScreen = ( { route, navigation } ) =>
   {
      let { nom, phone, messageries } = route.params;
      nom = nom.charAt( 0 ).toUpperCase() + nom.slice( 1 ).toLowerCase()
      const [messages, setMessages] = useState( messageries );
      const [results, setResults] = useState( [] );
      const [end, setEnd] = useState( false );
      const [begin, setBegin] = useState( true );
      const [userRef, setUserRef] = useState( route.params.userRef )
 
     const [isSuscribe, setIsSuscribe] = useState( false );
     // const [typeFournisseur, setTypeFournisseur] = useState( "" );
     const [problemeFournisseur, setProblemeFournisseur] = useState( "" );
     const [coutFournisseur, setCoutFournisseur] = useState( 0 );
     
     let typeFournisseur = ''
     let nameFournisseur = ''
     let problemeType = ''
     let prix = 0
     
 
     // TODO: setter le type de fournisseur: energie ? internet ? mobile ?
     // TODO: setter [probleme_type],  [fournisseur_name],  [cout]
     const sentences = {
      "name_fournisseur": `C'est noté ${ nom }! Pouvez vous me donner le nom de votre fournisseur ${ typeFournisseur } ? `,
      "nature_probleme": `Entendu ! Avez vous un problème de facture ? Un problème technique ? Un problème administratif ou un conflit avec votre fournisseur actuel ?`,
      "price": `Afin de vous proposer le meilleur service, pouvez vous me donner le prix moyen de votre abonnement mensuel ?`,
      "recapitulatif": `C'est noté ${ nom }, Votre demande concerne : ${ problemeType }, avec ${ nameFournisseur } et votre mensualité s'élève à ${ coutFournisseur } euros. C'est bien cela ?`,
      "conf_phone": `Afin d'être rappeler par nos conseillers, pouvez vous me confirmer votre numéro de téléphone: ${ phone }`,
      "no_conf_phone": `Les informations recueillis ne sont pas très claires, pas d’inquiétude ! Afin d'être rappeler par nos conseillers, pouvez vous me confirmer votre numéro de téléphone : ${ phone }`,
      "after_call": `Très bien ${ nom }, souhaitez vous être rappelé dans un instant ? Sinon, merci de nous préciser une heure aujourd'hui ?`,
      "now_call": `Très bien ${ nom }, un conseiller va vous rappeler. Merci de préparer une facture de votre fournisseur ou opérateur. À bientôt sur votre assistant Pioui.`,
      "no_response": "Je n'ai pas compris, pouvez vous reformuler votre réponse ?"
    }

     const words = {
      "problems": {
        "type": "problems",
        "response": `C'est noté ${ nom }! Pouvez vous me donner le nom de votre fournisseur ${ typeFournisseur } ? `,
        "words": ["un fournisseur", "énergie", "gaz", "électricité", "d'énergie", "web", "fournisseur", "fournisseur d'énergie", "opérateur internet", "internet", "mobile", "opérateur mobile", "smartphone", "téléphone"]
      },
      "another": {
        "type": "problems",
        "response": `C’est noté ${ nom }! Pouvez vous me donner le nom de votre prestataire ?`,
        "words": ["autre", "autre prestataire"]
      },
      "fournisseurs": {
        "type": "fournisseurs",
        "response": `Entendu ! Avez vous un problème de facture ? Un problème technique ? Un problème administratif ou un conflit avec votre fournisseur actuel ?`,
        "words": ["butagaz", "gdf", "la poste mobile", "fred", "raide", "billet new", "sache", "nrj", "c'est discount", "nrj mobile", "discount", "cdiscount", "edf", "high-tech", "hi like", "ilek", "targuas", "en targua", "antargaz", "gdf suez", "leclerc", "total énergie", "annie", "où est Anni", "tags as", "anni", "angie", "engie", "total", "total énergie", "total energie", "eni", "méga énergie", "ek water", "ekwater", "vattenfall", "c dsicount energie", "happ-e", "planète oui", "orange", "sfr", "free", "bouygues"]
      },
      "recapitulatif": {
        "type": "recapitulatif",
        "response": `Afin de vous proposer le meilleur service, pouvez vous me donner le montant de votre abonnement mensuel ?`,
        "words": ["un problème de", "réseau", "problème", "problème de", "facturation", "administratif", "facture", "technique", 'problème de facture', 'problème administratif', 'administration', 'problème technique', 'administrative', 'conflit', 'conflict']
      },
      "price": {
        "type": "price",
        "response": `C'est noté ${ nom }! votre demande concerne : ${ problemeType } avec ${ nameFournisseur } et votre mensualité s'élève à ${ prix }. C'est bien cela ?`,
        "words": ['euro', 'euros', '€'],
      },
      "non_recapitulatif": {
        "type": "non_recapitulatif",
        "response": `Les informations recueillis ne sont pas très claires, pas d’inquiétude ! Souhaitez vous être rappelé par un conseiller immédiatement ?  Sinon, merci de nous préciser une heure de préférence, aujourd'hui !`,
        "words": ["non", "nan", "pas vraiment", "pas", "point", "négatif", "je sais pas", "je ne crois pas", "pas vraiment", "c'est faux", "incorrect", "je ne veux pas", "c'est pas ça", "pas du tout"],
      },
      "oui_recapitulatif": {
        "type": "oui_recapitulatif",
        "response": `Très bien ${ nom }, souhaitez vous être rappelé dans un instant? Sinon, merci de nous préciser une heure aujourd'hui ?`,
        "words": ["oui", "correct", "bien", "bon", "exact", "OK", "c'est ça", "tout à fait", "ouais", "c'est bon", "C'est correct", "D'accord", "Bien sûr", "OK"],
      },
      "hour_conf": {
        "type": "hour_conf",
        "response": `Très bien ${ nom }!. Un conseiller va vous rappeler. Merci de préparer une facture de votre fournisseur ou opérateur. À bientôt sur votre assistant Pioui.`,
        "words": ["dans un instant", "maintenant", "immédiatement", "Tout de suite",  "vas-y", "je suis prêt", "now", "je veux bien"],
      },
      "after_conf": {
        "type": "hour_conf",
        "response": `Très bien ${ nom }!. Un conseiller va vous rappeler. Merci de préparer une facture de votre fournisseur ou opérateur. À bientôt sur votre assistant Pioui.`,
        "words": [ "tout à l'heure","ce soir", "plutôt", "à partir de","fin de journée", "plus tard","soirée",  "soirée",  "je ne veux pas", "non", "nan", "dans la soirée", "peux pas", "pas", "non", "20h"],
      },
      "not_found": {
        "type": "not_found",
        "response": `Désolé ${ nom }, je n'ai pas compris, pouvez vous reformuler votre réponse ?`,
        "words": [],
      },
      "stop_found": {
        "type": "stop_found",
        "response": `D'accord, je met en pause votre Assistant Pioui`,
        "words": ["top", "stop", "arrêtes"],
      },
      //  "oui": {
      //      "response": `Afin d'être rappeler par nos conseillers, pouvez vous me confirmer votre numéro de téléphone : [phone]`,
      //      "words" : ["oui", "c'est ça", "tout à fait", "C'est correct", "D'accord", "Bien sûr", "OK"],
      //   },
      //  "non": {
      //      "response": `Très bien ${ nom }!, Souhaitez-vous être rappeler dans un instant ? Sinon merci de nous préciser une heure aujourd'hui !`,
      //      "words" : ["pas question", "non", "je ne crois pas", "pas vraiment", "c'est faux", "incorrect",],
      //   },
     
    }
    

    /**
     * Init TTS
     */
    const initTts = async () =>
    {
      
  

      Tts.addEventListener( 'tts-start', async ( event ) => console.log("Go", event) );
      Tts.addEventListener( 'tts-finish', ( event ) => startRecognizing(end));
      Tts.addEventListener( 'tts-cancel', ( event ) => Tts.stop() );
      Tts.setDucking(false);
    }
    
     
    let stepping = 0;
    let nextStep = false;
    const onSend = useCallback((messages = []) => {
      setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
    }, [] )
    
    
    let currentStep = 0;
    let ensemble = ""
  
    let lastRes = ""
    let lastWord = ""
    let sentence = ""

    /**
     * On Speech result
     * @param {*} e 
     * @returns 
     */

     const onSpeechResults = ( e ) =>
    {
      // console.log( "******** All value ****************" )
      // console.log( e.value[0] )
      if ( e.value[0] && e.value[0].length > 2 )
      {
        sentence = e.value[0]
        const tab = sentence.toLowerCase().split( " " )
        lastWord = tab[tab.length - 1];
        ensemble = sentence.toLowerCase()
        
        console.log(sentence);
        console.log(ensemble);
                onSpeechResultsEnd()        
      }
    }

      

    /**
     *  On Speech end
     * @param {*} e 
     * @returns 
     */

     const onSpeechResultsEnd = async ( e ) =>
     {
       console.log( "***************** ENDING  laaaaaaaaaa!!!! *****************" )
       console.log(e)
       console.log( sentence )
       console.log( ensemble )
       
     
       console.log( "Stepping ++" )
       console.log(currentStep)
         const regex = new RegExp( lastWord, "i" )
             
         // const regexPrice = new RegExp( /([0-9.,]{1,5})?[ ]?(euros?|€)/, "i" )
         // let testprice = regexPrice.test( sentence )
             
         const regexHour = new RegExp( /[0-9]{1,2}[ ]?(h\.?|heure)([0-9]{0,2})/, "i" )
         let testhour = regexHour.test( ensemble )
 
         let lastStep = ""
         //Stop Intent
         if ( words.stop_found.words.find( value => ensemble.includes( value ) ) )
         {
           console.log( "Stop Intent" )
           Tts.stop();
           Tts.removeAllListeners( 'tts-finish' )
           Voice.stop()
           setResults( [] );
           setIsSuscribe( true )

           return destroyRecognizer( words.stop_found, sentence, lastWord )
           
         }
         else
         {
           
           await axios.post( `${ url }user/update-step`, {
             step: {
               sentence: ensemble
             },
             usersRef: userRef,
           } ).then( ( response ) =>
           {
           } )
             .catch( function ( error )
             {
               console.log( error );
             } );
           
             
           // First Intent: "fournisseur d'énergie", "opérateur internet", "opérateur mobile"
           if ( words.problems.words.find( value => ensemble.includes( value ) )
             && lastStep != words.problems.type && currentStep === 0
           )
           {
             lastStep = words.problems.type;
             let regexEnergie = /(fournisseur|energie|énergie|gaz|électricité)/ig;
             let regexInternet = /(internet|web)/ig;
             let regexMobile = /(mobile|smartphone|téléphone)/ig;
               
             if ( ensemble.match( regexEnergie ) )
             {
               typeFournisseur = 'Un fournisseur energie'
             } else if ( ensemble.match( regexInternet ) )
             {
               typeFournisseur = 'Un opérateur internet'
             } else if ( ensemble.match( regexMobile ) )
             {
               typeFournisseur = 'Un opérateur mobile'
             }
               
             await axios.post( `${ url }user/update-conversation`, {
               field: "fournisseur_type",
               valeur: typeFournisseur,
               usersRef: userRef,
             } )
               
             Voice.stop()
             currentStep++
               
             console.log( "Stpeing 0 zero..." )
             return destroyRecognizer( words.problems, sentence, lastWord, typeFournisseur )
               
           }
           else if ( words.fournisseurs.words.find( value => ensemble.includes( value ) )
              
           )
           {
             lastStep = words.fournisseurs.type
             console.log( "2 Intent" )
             currentStep++
             lastStep = words.problems.type;
             let regexEnergie = /(gdf|B&You|EDF|Ilek|CDiscount|planète oui|ENGIE|Antargaz|targuas|gdf suez|Total|Total énergie|TOTAL ENERGIE|ENI|HAPP-E|méga énergie|MEGA ENERGIE|EKWATER|VATTENFALL|ek water|Barry|Leclerc|Leclerc énergie|Elécocité|Butagaz|Ohm Energie|Ovo energy)/ig;
             let regexMobile = /(la poste mobile|red|orange|sosh|free|sfr|nrj|nrj mobile|bouygues|Bouygues Télécom|Prixtel|NRJ Mobile|Sosh|B&You|Red|La Poste Mobile)/ig;
             let regexInternet = /(orange|free|sfr|bouygues|Bouygues Télécom|Les Furets|B&You|Sosh|Red|La Poste Internet)/ig;
               
             ensemble = ensemble.replace( /bouygues/i, "Bouygues Télécom" )
   
             ensemble = ensemble.replace( /Angie/i, "Engie" )
             ensemble = ensemble.replace( /Annie|Où est Anni|Anni/i, "ENI" )
             ensemble = ensemble.replace( /tags as/i, "Butagaz" )
             ensemble = ensemble.replace( /en targua/i, "Antargaz" )
             ensemble = ensemble.replace( /hi like/i, "Ilek" )
             ensemble = ensemble.replace( /high-tech/i, "Ilek" )
             ensemble = ensemble.replace( /c'est discount/i, "CDiscount" )
             ensemble = ensemble.replace( /sache/i, "Sosh" )
             ensemble = ensemble.replace( /billet new/i, "B&You" )
             ensemble = ensemble.replace( /raide|fred/i, "RED" )
   
             if ( nameFournisseur = ensemble.match( regexEnergie ) )
             {
               nameFournisseur = nameFournisseur[0]
             } else if ( nameFournisseur = ensemble.match( regexInternet ) )
             {
               nameFournisseur = nameFournisseur[0]
             } else if ( nameFournisseur = ensemble.match( regexMobile ) )
             {
               nameFournisseur = nameFournisseur[0]
             }
               
             await axios.post( `${ url }user/update-conversation`, {
               field: "fournisseur_name",
               valeur: nameFournisseur,
               usersRef: userRef,
             } )
               
             console.log( "*** *** *** ensemble ****** *** **" )
             console.log( ensemble )
             console.log( "*** *** *** nameFournisseur ****** *** **" )
             console.log( nameFournisseur )
               
             Voice.stop()
               
             return destroyRecognizer( words.fournisseurs, sentence, lastWord, nameFournisseur )
               
           }
               
           // Second Intent: "edf", "engie", "total energie"
           else if ( words.another.words.find( value => ensemble.includes( value ) )
             && lastStep != words.another.type
           )
           {
             lastStep = words.another.type
             currentStep++
               
               
             await axios.post( `${ url }user/update-conversation`, {
               field: "fournisseur_type",
               valeur: "autre",
               usersRef: userRef,
             } )
   
             let nameFournisseur = "Autre prestataire"
             Voice.stop()
             return destroyRecognizer( words.another, sentence, lastWord, nameFournisseur )
           }
               
           // Third Intent: 'problème de facture', 'problème administratif', 
           else if ( ( words.recapitulatif.words.find( value => value === lastWord ) ||
             words.recapitulatif.words.find( value => ensemble.includes( value ) )
           )
             && lastStep != words.recapitulatif.type
           )
           {
               
             let regexFacture = /(facture|facturation|facturette)/ig;
             let regexAdmin = /(administratif|administrative|administration)/ig;
             let regexTechnique = /(technique|réseau)/ig;
             let regexConflit = /(conflit|conflictuel|probleme)/ig;
               
             if ( problemeType = ensemble.match( regexFacture ) )
             {
               problemeType = "Un problème de facture"
             } else if ( problemeType = ensemble.match( regexAdmin ) )
             {
               problemeType = "Un problème administratif"
             } else if ( problemeType = ensemble.match( regexTechnique ) )
             {
               problemeType = "Un problème technique"
             }
             else if ( problemeType = ensemble.match( regexConflit ) )
             {
               problemeType = "Un conflit"
             } else
             {
               problemeType = "Autre problème"
             }
               
             await axios.post( `${ url }user/update-conversation`, {
               field: "probleme_type",
               valeur: problemeType,
               usersRef: userRef,
             } )
               
             lastStep = words.recapitulatif.type
             console.log( "3 Intent" )
             currentStep++
             return destroyRecognizer( words.recapitulatif, sentence, lastWord, problemeType )
               
           }
           // Four Intent: 'euro', '€'
           else if ( ( words.price.words.find( value => ensemble.includes( value ) ) )
             && lastStep != words.price.type )
           {
             // let regexprice = /([0-9.,]{1,5})?[ ]?(euros?|€)/ig;
             // prix = ensemble.match( regexprice )
             prix = ensemble
             await axios.post( `${ url }user/update-conversation`, {
               field: "cout",
               valeur: ensemble,
               usersRef: userRef,
             } )
               
             lastStep = words.price.type
             console.log( "4 Intent" )
             currentStep++
               
             if ( nameFournisseur === "Autre prestataire" || nameFournisseur === "" )
             {
               nameFournisseur = "un autre prestataire"
             }
             let ph = `C'est noté ${ nom } votre demande concerne :  ${ problemeType } avec ${ nameFournisseur } et votre mensualité s'élève à ${ prix }. C'est bien cela?`;
             return destroyRecognizer( words.price, sentence, lastWord, prix, ph )
               
           }
               
           // Five Intent: Confirmation 'oui'
           else if ( words.oui_recapitulatif.words.find( value => ensemble.includes( value ) )
             && currentStep === 4 && lastStep != words.oui_recapitulatif.type )
           {
             lastStep = words.oui_recapitulatif.type
             console.log( currentStep )
             console.log( "5 Intent" )
             currentStep++
   
             return destroyRecognizer( words.oui_recapitulatif, sentence, lastWord, "Oui" )
           }
           // Six Intent: Rappel 'non'
           else if ( words.non_recapitulatif.words.find( value => ensemble.includes( value ) )
             && currentStep === 4
             && lastStep != words.non_recapitulatif.type )
           {
               
             lastStep = words.non_recapitulatif.type
             console.log( currentStep )
             console.log( "6 Intent" )
             currentStep++
             return destroyRecognizer( words.non_recapitulatif, sentence, lastWord, "Non" )
           }
              // Seven Intent: End '18h', 'mainteant', 'tout de suite'
           else if ( words.hour_conf.words.find( value => ensemble.includes( value ) )
             && lastStep != words.hour_conf.type && currentStep >= 5 )
           {
             lastStep = words.hour_conf.type
             console.log( "7 Intent" )
             currentStep++
             setEnd( true )
             setIsSuscribe( false )
             Tts.stop();
             Tts.removeAllListeners( 'tts-finish' )
             Voice.stop()
             setResults( [] );
             setIsSuscribe( false )
               
             await axios.post( `${ url }user/update-conversation`, {
               field: "due_date",
               valeur: "Maintenant",
               usersRef: userRef,
             } )
               
             destroyRecognizer( words.hour_conf, sentence, lastWord, "Maintenant" )
             currentStep = 0;
             //  await AsyncStorage.removeItem(
             //   'user'
             // );
             // navigation.navigate( 'Success',  {
             //     nom ,
             //     phone,
             // })
           }
           else if ( ( words.after_conf.words.find( value => ensemble.includes( value ) ) || testhour )
             && lastStep != words.after_conf.type && ( currentStep >= 5 ) )
           {
             lastStep = words.after_conf.type
             console.log( "7 Intent" )
             currentStep++
             setEnd( true )
             setIsSuscribe( false )
             Tts.stop();
             Tts.removeAllListeners( 'tts-finish' )
             Voice.stop()
             setResults( [] );
             setIsSuscribe( false )
               
             let phrase = "Plus tard"
               
             if ( testhour )
             {
               let hour = ensemble.match( regexHour )
               phrase = hour[0]
             }
               
               
             await axios.post( `${ url }user/update-conversation`, {
               field: "due_date",
               valeur: phrase,
               usersRef: userRef,
             } )
             destroyRecognizer( words.after_conf, sentence, lastWord, phrase )
             currentStep = 0;
             // await AsyncStorage.removeItem('user');
             // navigation.navigate( 'Success',  {
             //     nom,
             //     phone,
             // })
               
           }
           else
           {
             console.log( "currentStep" )
             console.log( currentStep )
               
             if ( currentStep === 1 || currentStep === 2 )
             {
                 
               await axios.post( `${ url }user/update-conversation`, {
                 field: "fournisseur_name",
                 valeur: sentence,
                 usersRef: userRef,
               } )
               currentStep++
               lastRes = sentence
               return destroyRecognizer( words.fournisseurs, sentence, lastWord )
             }
               
           
               
             else if ( sentence.length > 8 && lastRes != sentence )
             {
               lastRes = sentence
               console.log( "No Intent !!!!" )
               console.log( "default Intent" )
               return destroyRecognizer( words.not_found, sentence, lastWord )
             }
               
           }
             
         }
   };

  /**
   * Start Recording
   * @param {*} ending 
   */

   const startRecognizing = async (ending = false) =>
    {
     
        //Starts listening for speech for a specific locale
        try
        {
          await Tts.stop()
          setIsSuscribe( true )
          await Voice.start( 'fr-FR' );
          setResults( [] );
          const timer = setTimeout( async () =>
          {
              clearTimeout( timer );
              await Voice.stop()
              setIsSuscribe( false )
          }, 5000) // 5 secondes de paroles
      
        } catch ( e )
        {
          //eslint-disable-next-line
          console.error( e );
        }
        
    };

  let lastResponse = ""

  /**
   * Destroy recognizer
   * @param {*} intent 
   * @param {*} sentences 
   * @param {*} lastWord 
   * @param {*} tts 
   * @param {*} override 
   */
   const destroyRecognizer = async ( intent, sentences = "", lastWord = "", tts = "", override = "" ) =>
   {
   
     console.log( "DEBUG *****" )
     console.log( intent, sentences, lastWord, tts, override )
     try
     {
       await Voice.stop();
       setIsSuscribe( false )
       lastIntent = ""
     
       if ( intent.type != lastIntent )
       {
       
         if ( intent.type === "not_found" )
         {
           lastIntent = intent.type
           lastResponse = "???"
           onSend( {
             _id: Math.floor( Math.random() * 1000 ),
             text: "???",
             inverted: true,
             position: 'right',
             createdAt: new Date(),
             user: {
               _id: 20,
               name: 'Vous',
               avatar: 'https://p.kindpng.com/picc/s/78-785827_user-profile-avatar-login-account-male-user-icon.png',
             },
           } )
       
         } else
         {
        
           if ( tts != "" )
           {
             console.log( "Gooo TTS Bullet ****" )
             console.log( tts )
             onSend( {
               _id: Math.floor( Math.random() * 1000 ),
               text: tts,
               inverted: true,
               position: 'right',
               createdAt: new Date(),
               user: {
                 _id: 20,
                 name: 'Vous',
                 avatar: 'https://p.kindpng.com/picc/s/78-785827_user-profile-avatar-login-account-male-user-icon.png',
               },
             } )
           }
       
         }
       
         if ( lastResponse != intent.response )
         {
           let thingToSay = ""
           if ( override == "" )
           {
             console.log( "Gooo intent response Bullet ****" )
             console.log( intent.response )
             thingToSay = intent.response
             onSend( {
               _id: Math.floor( Math.random() * 1000 ),
               text: intent.response,
               inverted: false,
               position: 'left',
               createdAt: new Date(),
               user: {
                 _id: 2,
                 name: 'Pioui',
                 avatar: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTExYSExESEhYWFhEQEBEQEBAQERYRFhYXFxYTFhYZHioiGRsnHhYWIzMjJystMDAwGCE2OzYvOiovMC0BCwsLDw4PGxERGC8nIScvLy8vMC8vLy8vLy8tLy8vLy8vLy8vLy0tLy8vLy8vLS8vLy8vLy8vLy8vLy8vLy8vL//AABEIANcA6gMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABQIDBAYHAQj/xAA9EAABAwIDBQUECQMEAwAAAAABAAIDBBEFEiEGMUFRYRMicYGRBzJSsRQjQmJykqHB0UPh8IKissIVM5P/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAgMEBQEG/8QAMBEAAgEDAgMHAwQDAQAAAAAAAAECAwQREiExQVETImGBkcHwBXHRMlKhsWLh8UL/2gAMAwEAAhEDEQA/AO4oiIAiIgCIiAIiIAiIgCIiAIiIAijcTxmKAd9wvwHH0Wj4t7Sy0kRRX6u0VsKM58EVTrQhxZ0pFxeT2nVV/cZ+Y/wsmj9q8gP1sNxzY4H9CFa7SouhSryn4nX0WpYDt5TVJDc2R/wu7p8r7/JbUx4IuDccws84Sg8SRohUjNZi8laIiiTCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiA8c6ywK7tXNPZ2B4Emx8v5Kyjrrw4K05r8187cnwdmc1+efN+yktmRe5yPaCofnc03uCQ6++/EeK12WnW6bcwgVLyOOUnxLWkrD2ZwT6RLY3DG96Rw5ch1P8AK68JJQ1M5dWLlPSiHwfZGap1AysG97rho6dT0CmJdgIWjWV7jzaxoH63XTI4WtaGNaGtaLNaNwCwa9oAJsfBrXPPo0ErI7iUntsaI28Et9zjmL7MPh70bs4Gu4teOtuPktg2F26kicIZ3F7NAHE3I8+IWwVbGvbdpuNeBBBG8EHUHoVzzaPD+zf2jBa57wHPmr4tVFpmZ6kHSeuB9CQTNe0Oabg6ghXVzP2Y7Rlw7B53atvy4j9/VdMXOq03Tlg6NGqqsNSCIirLQiIgCIiAIiIAiIgCIiAIiIAiIgCIiAKh+unqqibKlAUuKsRVDXe6QeoII5LTvapj5p6cRMcWvmzNu02dkHvW6m4HmVi7A3pMM7R+hJlcxvNznHKB0uLq5U2458ipzWrHmR+10wfUSEcHFv5QG/stv2UoOxp2aWdIBK/n3h3R5C3qVz2U53a8TYnxXQdscUfTUz3wgGUmOmpmnd20rxHHfoCb/wCla7nuwjH5sZaHenKXzcoxramngk7EmSaYi4p6aJ881jxc1vuDq4hRUu2kTNZ6atpG7u1npj2Q1sC58ZcGDxspfAsEjo4cje8931lRO/WSWU6vle7edb2HDQLKpMRY4uYW2FrkusQ4biCPPcse5rxki6nK9okYWua4BzXsIc1zSNHAjeLcVpW0EANwdxuCpmGmFFW/Roxlpqpk1RTRX0hniLTNHGPsxua8PtuBvZYWPM1K00ZGerFGlYFWOgma4Gxa6x8ivoXCasSxteOIB8ivnGuFpT5Fdj9mmI54A0nddv8ACsuoaoKXQy2c9FRw6m8IiLmnWCIiAIiIAiIgCIiAIiIAiIgCIiAIiICg7/D5rwo03F+d14/xsvc4PDTNtNjPptRTyulDI4g5skYBzuBcD3TubuUdtKTdrM8bIoxljiYSbNAtdxta+nkt1xOjc9jgx1nWOUO90nkSNR4ri+PYowSOhlEtPILj6wZ4r3se83UehWSpdXUJJRSS5c/n3NtpZW9fLqPpnl9s88GZJUMGmZviSLLb9sJjNh7KlgL+xkpa5zW6kiCRrpQPAB58lzNtO5+rbSD4onCQf7dfUKdwTG6ikaQ3WPUuZO09nrvte1v80V8b+VSOmrjK5rb1XuvQV/pdOnLVbt/Z7+j9n6nRtosZhjpmziRvYvDZBKDdrmEZmkW330sFBbEdrUuNVI0xx6tp43byCReR3oAPPxMVs/glXJAXwxUj6V8hkhw6ubI6FvOSCSxMTS65DSCN9rXWwSuxd7cjIaGhFrGbtpK2Rotvjjytbf8AEbK+M+7sYpww8Pka77TT9Jq6eCOV7PouSWeSI2cx1VNBDFHm4OIzutysru0B1KuYphTKanytL3vfU0cs08pzSyymphvI93yG4LBxubetFGOGZqryjRcTd9aegA/z1W9+yytyvcy/Ihc5mkzOLuZJ8uC2bYeqyTt6/t/hW2SzBo5iliqpeJ9AA3F/NVKzSuu0K8uNwO4nkIiIehERAEREAREQBERAEREAREQBWaqTK0nyCuE21Oij5JRNYMPd7xLugNrj9VKKyeN4LuGy5ox0LmnyKvlI4w0BoFgNAEQieXXFvbhRhk0UoFs4cHeNv7FdpWsbZ7JtrgwEsGS9s7HOHQixHM6dVVWTaTSy08+z/hmm0moVO88JrH48eJ84wte82jBJ5jQDxPBdO2C9n75C2eqzFmhAdcF/Rt9cvN3HhzG+YDsLTU9nEds8agyNb2bT92MaeZuVszlGFFvefp+S+vdxW1N5fXh6L3foG2AAAAAAAAFgANAAFjVMirkeorEJ9CtcY7nNbNY22qB2Dz8OSX/5va//AKrSdqK/QsG83v8Ah/up/aaZ2R4LS4FrmkDiCLH5rnUlQ52jrl/uuNjbTTPfda2v6LbTjjiY6sm9kUtNwDuuAVK4FLlmYfvAeuiiyr9K+xB5EFaUjBPqfSWBTZomn7o9dxUktY2Iqg+Fvn+outnXGqLEmdyk8wTCIigWBERAEREAREQBERAEREAVmoqGxtzPcGgcSofH9poaYG5Dn/CD81yjaTbWWYnWw4DgPBaKVvKe72RRUrqOy3ZuG1W2F7xxnQ6Bo95x4X/hbvhVN2cbW8Q1rT/pFvnc+a4FsiDU19PGbkGUPd+GMGQ/8V9E24KdwoxShEhb6pNzkeFUKtULKjQyleoUUjwpKtvKrKtPK9R4WJSoyrZdSEhWHOrIkGavilMDfRaHjuGWJc0a8Rz/ALrpddGtVxeLetVORmqRTOeFVxlZOJwZXXHHf4rDBWnJhkjqfsyxwNHZOOoItddVikDhcL5lw6tdG4OadR8l1bZPbFrgGvdY7tf36LJcUNT1xNdpcJLRI6Ois09S14u0g+avLnnTCIiAIiIAiIgCIiAK1PHmaW3Lbgi7TZw8CrqIDSK/2cQSkuM81zrqWO/ZR7vZJTnfUS/lYujore3qfuKuwp9DTtndgaejlbPG6RzwHNGctygOGpsBvW4FW3e80fiKuFQlJy3bJxio7IoKpVRVKBnhQovCvTwpcrLyrrisd5UkRLEhWFOVlylYM5ViIswahaxi43rZKl61vFSroFMjT8UjvdQTVsGIKBlHeK0oxzW5S1yy4Kgg3BIPMK2ymurjaF/BeqRRKOTZ8E2xngI72YciV0PBfaJBJZsl4zzOo9Vxf6HKPslVNjlH2HeijOnTqfqROnVrU/0vyZ9KUlbHILxva8fdIKyV83UlXURm7A9p5tutmwvaLFTYRmV/Qxh7fUhZpWX7Z+prjfv/AN035f7wdrRQ2zD6p0IdVhrZDua0AEN624qZWOUdLaz6HQhLVFPGPuERFEkEREAREQBERAY73d9o6O/ZXisOsdaWHqXj/asouUny+c2eLizwrwlCVSSh4wvChKpJUjwpeVYeVceVjyFSSImPMVgVD1lTvUVVSqyKK2YlXIoDEX6KTqpVAYhKr4oqkQNe5Qk3vKUrXqKf7ytRlqGZRzFu8Bw5G/zC6dsrskypp2VAcY8xeMh71sri3f5LmELF33YCAsoYQeIe78z3H91G5k4QyupGzip1WmuX4MKPYaMb3k+SyotjKcb8x81sqLnutPqdZUYLkRNPs9Ts3RN8SLn9VJRwtbuaB4BXEUXJviySjFcEERFEkEREAREQBERAEREBE48/I2OT4JGE/hJsVmyusreL0/aQyM4lpt4jUfJYGGVnawMfxtkf+Nvdd8r+asW8V4EM4kZgmVYkCj5JFT2690gki5UlywfpCGoXuDwyXvWLNIrb6hYU9QppEGymqmUPVSq9UzqLqZVbFFbMeqlUBXyqRq5lAVsytRVIj6p6wI9TdXql68p2KyKMlWWES+DUhkkawfaIHlxX0HhUAjijYBazWi3kuW+zbBy+TtCNG2aPE7z6LryzXs8tR6Gn6dTxF1Hz/oIiLCdIIiIAiIgCIiAIiIAiIgCIiALTIZvo1Y+mdpHUfXU5O7tNzmedv0HNbmtd2zwH6XAWsOWVh7SB97EPH2b8Abb+BseCspSSeJcH8z5f0QqJ4yuK+fPEpqXWWG6Za9gG1nbH6PUjsqhpMZzd0SOGhH3ZOY48OSkJ5bFX6HF4ZUqiksoz+3VDqlRZqlQape6RqJJ9SsWaoWE+pWLNVBSwQbL1ROoypnVuoq1FVVSppEGzysqFC1MqvVM6jZX3Nh5qxFM5YWWUe8VL4RQue4AC5JAA6rEoqUuK6xsBs1ltM9uv2ARuHNTclTjqZkUHXnpXn9ja9lsJEELW21A1PNx3lTipa2wsqlyZScm2zuwioxUUERFEkEREAREQBERAEREAREQBFbfMAsSavAXuAZ6pLxzUP9Pc42brztuA5k8Fg4hibGDvPLz8LDZvm7+FGTUVmTPYxcniKya/7StmYZwZ43MZOB3mkhrJGjmdweBuPHceFtFwvauRgEdRmkaNA/8AqNHI398eOvUrY9oMUdKC0ANHTT+58StDroyDff8A5zU6N/D9E1t15osqfS6rXaU33unJ+fX54m7RYgyQZo3h46HUdCN481S+pXP2kg3a4h3QlpWbHjEw32f1cLH1C6KipLVF5RzG5RlomsPobZJVrEmq1BHGSd8Z8nAqy/EydzD5kJgORJz1Sj6ip6rEfPI7cAP1XsVA5x1uV7grlLYsSSl2g9Vl0NCXcNFOYRs0+Q91hPMnRo6klbzs1gdMxwzyMleODSDGDyvucV7OpCku8yiNOdd4ijF2L2QLiJJG2aNWgjf1XT4IQwWAVED2gWAt0tZZC59Wq6j3OlQoRpRwgiIqi8IiIAiIgCIiAIiIAiIgCwqqqsst40UTVwleo8ZgVdeVHwSGWURl1r5jv1IaLm3M/wArJnpionEcJ7S2pa5puxzdCCrMbbEOe5MVUhDcoGVvwj5k8T1K1rEXk3sr4fXtFu2imA3CeEOd+YWKxnYvkIbUwdjc2EsZLob/AHgdW+NyuLXt7pZljP24+h2LevQTS/K/tELPSuO9RtXShbpV0wtcW5gjUWWuYhEsEKjZ2qc9Rp9dTi6sxg8dfG5UtU0+qzdm8G7aRznA5I/1edzfIanxC6lrObmlCTXiuhVfQoRoynVin0TWd+RCthPFv5VfEcYGZwfYanK0H91vBwNnJUy4IwtItvBC+g7VnxfZQ8fJs05lfSt/pzP6fVtHzWXT7RD+lSxjk6UulPoLBQ1RTFj3NO9pLf4PpZZFEyxXIq39fDWcPwR9PR+h2bSnjUn1bfuS8mITy6SSOy/A2zWflbv87rPw+XLa12q1QxXUxDh19wXIlcSbzJ5N7pUYR0KOF4bfwtifwjGJAACcw630/cLaKOvDrWPkd/lz+a0ykoC0XJAaN5cQ1o8zuWRLtDRwC8lVHcfZjcJH36ZdPUq+heT4JZORcW1Nvu/wb5HKCri1PZjaIVQe8RujaHdzNvcyw73Q3ufNbNFLddhrBysl5EReHoREQBERAEREAREQBUOjBVaIDFfRgrHdhwUki9yeYIk4aOSsVeDMkaWPaHNIIIKnV5Ze6mMI5GyOSildSzXMd700p3ZD/TJ+SVlPm3AnwBK6dimExVDckrA4cLjULXpNhmkZW1dUxnwMe0i3IFwJC5tzY9pPtKbSb4r3Ona36px0z5c/z8+++759/wCPfJKIIgHSO1PFsbOMklt3QbyVvWHYKyCJsTBo3e4+85x1c89SVsGE4BBTMyxMy3N3uJLpHu+J7jq4rJfSLXbUVRj4lF5dyuH/AIrgvc1p1IrTqVbE+k6Kw+lWrUYcHLts8IyPEwHd3PsOHxeXyUZT0vHzB6LrNbhgkYWOGhXOq3D30Ty10bnwk3GX/wBjPw829FgvLeU+/T4817nY+nX/AGceyqPbk/b8f8MjDoFm4zjcdJFnIzPPdij4ud16KMfj9Oxt2Oke7gwREOvyvdebO7PTVs4qKhpbGyxZGd1huHmsFrYzq1NVVNQXHx8C++u4wWE8y5L3ZZo9l6rELS1czmtOrItcoHJrdwWwYb7OqWMgkOeRzsAt2jpraAeAWTHTrvRaisRWF4HBk3N5k8/PnAw6KkbG0NY0NHIKRiCrjp1fbHZQyepHsZVa8AXqiehERAEREAREQBERAEREAREQBERAEREAREQHhCoMQREBQacLFq8MZILPaD80Re5PMEbDsfTNdm7JpO/Xd6KYjpQBYAAcgLBEXrbfEJJF5sIVwCyIonp6iIgCIiAIiIAiIgP/2Q==',
               },
             } )
           } else
           {
             thingToSay = override
             console.log( "Gooo Override ****" )
             console.log( override )
             onSend( {
               _id: Math.floor( Math.random() * 1000 ),
               text: override,
               inverted: false,
               position: 'left',
               createdAt: new Date(),
               user: {
                 _id: 2,
                 name: 'Pioui',
                 avatar: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTExYSExESEhYWFhEQEBEQEBAQERYRFhYXFxYTFhYZHioiGRsnHhYWIzMjJystMDAwGCE2OzYvOiovMC0BCwsLDw4PGxERGC8nIScvLy8vMC8vLy8vLy8tLy8vLy8vLy8vLy0tLy8vLy8vLS8vLy8vLy8vLy8vLy8vLy8vL//AABEIANcA6gMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABQIDBAYHAQj/xAA9EAABAwIDBQUECQMEAwAAAAABAAIDBBEFEiEGMUFRYRMicYGRBzJSsRQjQmJykqHB0UPh8IKissIVM5P/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAgMEBQEG/8QAMBEAAgEDAgMHAwQDAQAAAAAAAAECAwQREiExQVETImGBkcHwBXHRMlKhsWLh8UL/2gAMAwEAAhEDEQA/AO4oiIAiIgCIiAIiIAiIgCIiAIiIAijcTxmKAd9wvwHH0Wj4t7Sy0kRRX6u0VsKM58EVTrQhxZ0pFxeT2nVV/cZ+Y/wsmj9q8gP1sNxzY4H9CFa7SouhSryn4nX0WpYDt5TVJDc2R/wu7p8r7/JbUx4IuDccws84Sg8SRohUjNZi8laIiiTCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiA8c6ywK7tXNPZ2B4Emx8v5Kyjrrw4K05r8187cnwdmc1+efN+yktmRe5yPaCofnc03uCQ6++/EeK12WnW6bcwgVLyOOUnxLWkrD2ZwT6RLY3DG96Rw5ch1P8AK68JJQ1M5dWLlPSiHwfZGap1AysG97rho6dT0CmJdgIWjWV7jzaxoH63XTI4WtaGNaGtaLNaNwCwa9oAJsfBrXPPo0ErI7iUntsaI28Et9zjmL7MPh70bs4Gu4teOtuPktg2F26kicIZ3F7NAHE3I8+IWwVbGvbdpuNeBBBG8EHUHoVzzaPD+zf2jBa57wHPmr4tVFpmZ6kHSeuB9CQTNe0Oabg6ghXVzP2Y7Rlw7B53atvy4j9/VdMXOq03Tlg6NGqqsNSCIirLQiIgCIiAIiIAiIgCIiAIiIAiIgCIiAKh+unqqibKlAUuKsRVDXe6QeoII5LTvapj5p6cRMcWvmzNu02dkHvW6m4HmVi7A3pMM7R+hJlcxvNznHKB0uLq5U2458ipzWrHmR+10wfUSEcHFv5QG/stv2UoOxp2aWdIBK/n3h3R5C3qVz2U53a8TYnxXQdscUfTUz3wgGUmOmpmnd20rxHHfoCb/wCla7nuwjH5sZaHenKXzcoxramngk7EmSaYi4p6aJ881jxc1vuDq4hRUu2kTNZ6atpG7u1npj2Q1sC58ZcGDxspfAsEjo4cje8931lRO/WSWU6vle7edb2HDQLKpMRY4uYW2FrkusQ4biCPPcse5rxki6nK9okYWua4BzXsIc1zSNHAjeLcVpW0EANwdxuCpmGmFFW/Roxlpqpk1RTRX0hniLTNHGPsxua8PtuBvZYWPM1K00ZGerFGlYFWOgma4Gxa6x8ivoXCasSxteOIB8ivnGuFpT5Fdj9mmI54A0nddv8ACsuoaoKXQy2c9FRw6m8IiLmnWCIiAIiIAiIgCIiAIiIAiIgCIiAIiICg7/D5rwo03F+d14/xsvc4PDTNtNjPptRTyulDI4g5skYBzuBcD3TubuUdtKTdrM8bIoxljiYSbNAtdxta+nkt1xOjc9jgx1nWOUO90nkSNR4ri+PYowSOhlEtPILj6wZ4r3se83UehWSpdXUJJRSS5c/n3NtpZW9fLqPpnl9s88GZJUMGmZviSLLb9sJjNh7KlgL+xkpa5zW6kiCRrpQPAB58lzNtO5+rbSD4onCQf7dfUKdwTG6ikaQ3WPUuZO09nrvte1v80V8b+VSOmrjK5rb1XuvQV/pdOnLVbt/Z7+j9n6nRtosZhjpmziRvYvDZBKDdrmEZmkW330sFBbEdrUuNVI0xx6tp43byCReR3oAPPxMVs/glXJAXwxUj6V8hkhw6ubI6FvOSCSxMTS65DSCN9rXWwSuxd7cjIaGhFrGbtpK2Rotvjjytbf8AEbK+M+7sYpww8Pka77TT9Jq6eCOV7PouSWeSI2cx1VNBDFHm4OIzutysru0B1KuYphTKanytL3vfU0cs08pzSyymphvI93yG4LBxubetFGOGZqryjRcTd9aegA/z1W9+yytyvcy/Ihc5mkzOLuZJ8uC2bYeqyTt6/t/hW2SzBo5iliqpeJ9AA3F/NVKzSuu0K8uNwO4nkIiIehERAEREAREQBERAEREAREQBWaqTK0nyCuE21Oij5JRNYMPd7xLugNrj9VKKyeN4LuGy5ox0LmnyKvlI4w0BoFgNAEQieXXFvbhRhk0UoFs4cHeNv7FdpWsbZ7JtrgwEsGS9s7HOHQixHM6dVVWTaTSy08+z/hmm0moVO88JrH48eJ84wte82jBJ5jQDxPBdO2C9n75C2eqzFmhAdcF/Rt9cvN3HhzG+YDsLTU9nEds8agyNb2bT92MaeZuVszlGFFvefp+S+vdxW1N5fXh6L3foG2AAAAAAAAFgANAAFjVMirkeorEJ9CtcY7nNbNY22qB2Dz8OSX/5va//AKrSdqK/QsG83v8Ah/up/aaZ2R4LS4FrmkDiCLH5rnUlQ52jrl/uuNjbTTPfda2v6LbTjjiY6sm9kUtNwDuuAVK4FLlmYfvAeuiiyr9K+xB5EFaUjBPqfSWBTZomn7o9dxUktY2Iqg+Fvn+outnXGqLEmdyk8wTCIigWBERAEREAREQBERAEREAVmoqGxtzPcGgcSofH9poaYG5Dn/CD81yjaTbWWYnWw4DgPBaKVvKe72RRUrqOy3ZuG1W2F7xxnQ6Bo95x4X/hbvhVN2cbW8Q1rT/pFvnc+a4FsiDU19PGbkGUPd+GMGQ/8V9E24KdwoxShEhb6pNzkeFUKtULKjQyleoUUjwpKtvKrKtPK9R4WJSoyrZdSEhWHOrIkGavilMDfRaHjuGWJc0a8Rz/ALrpddGtVxeLetVORmqRTOeFVxlZOJwZXXHHf4rDBWnJhkjqfsyxwNHZOOoItddVikDhcL5lw6tdG4OadR8l1bZPbFrgGvdY7tf36LJcUNT1xNdpcJLRI6Ois09S14u0g+avLnnTCIiAIiIAiIgCIiAK1PHmaW3Lbgi7TZw8CrqIDSK/2cQSkuM81zrqWO/ZR7vZJTnfUS/lYujore3qfuKuwp9DTtndgaejlbPG6RzwHNGctygOGpsBvW4FW3e80fiKuFQlJy3bJxio7IoKpVRVKBnhQovCvTwpcrLyrrisd5UkRLEhWFOVlylYM5ViIswahaxi43rZKl61vFSroFMjT8UjvdQTVsGIKBlHeK0oxzW5S1yy4Kgg3BIPMK2ymurjaF/BeqRRKOTZ8E2xngI72YciV0PBfaJBJZsl4zzOo9Vxf6HKPslVNjlH2HeijOnTqfqROnVrU/0vyZ9KUlbHILxva8fdIKyV83UlXURm7A9p5tutmwvaLFTYRmV/Qxh7fUhZpWX7Z+prjfv/AN035f7wdrRQ2zD6p0IdVhrZDua0AEN624qZWOUdLaz6HQhLVFPGPuERFEkEREAREQBERAY73d9o6O/ZXisOsdaWHqXj/asouUny+c2eLizwrwlCVSSh4wvChKpJUjwpeVYeVceVjyFSSImPMVgVD1lTvUVVSqyKK2YlXIoDEX6KTqpVAYhKr4oqkQNe5Qk3vKUrXqKf7ytRlqGZRzFu8Bw5G/zC6dsrskypp2VAcY8xeMh71sri3f5LmELF33YCAsoYQeIe78z3H91G5k4QyupGzip1WmuX4MKPYaMb3k+SyotjKcb8x81sqLnutPqdZUYLkRNPs9Ts3RN8SLn9VJRwtbuaB4BXEUXJviySjFcEERFEkEREAREQBERAEREBE48/I2OT4JGE/hJsVmyusreL0/aQyM4lpt4jUfJYGGVnawMfxtkf+Nvdd8r+asW8V4EM4kZgmVYkCj5JFT2690gki5UlywfpCGoXuDwyXvWLNIrb6hYU9QppEGymqmUPVSq9UzqLqZVbFFbMeqlUBXyqRq5lAVsytRVIj6p6wI9TdXql68p2KyKMlWWES+DUhkkawfaIHlxX0HhUAjijYBazWi3kuW+zbBy+TtCNG2aPE7z6LryzXs8tR6Gn6dTxF1Hz/oIiLCdIIiIAiIgCIiAIiIAiIgCIiALTIZvo1Y+mdpHUfXU5O7tNzmedv0HNbmtd2zwH6XAWsOWVh7SB97EPH2b8Abb+BseCspSSeJcH8z5f0QqJ4yuK+fPEpqXWWG6Za9gG1nbH6PUjsqhpMZzd0SOGhH3ZOY48OSkJ5bFX6HF4ZUqiksoz+3VDqlRZqlQape6RqJJ9SsWaoWE+pWLNVBSwQbL1ROoypnVuoq1FVVSppEGzysqFC1MqvVM6jZX3Nh5qxFM5YWWUe8VL4RQue4AC5JAA6rEoqUuK6xsBs1ltM9uv2ARuHNTclTjqZkUHXnpXn9ja9lsJEELW21A1PNx3lTipa2wsqlyZScm2zuwioxUUERFEkEREAREQBERAEREAREQBFbfMAsSavAXuAZ6pLxzUP9Pc42brztuA5k8Fg4hibGDvPLz8LDZvm7+FGTUVmTPYxcniKya/7StmYZwZ43MZOB3mkhrJGjmdweBuPHceFtFwvauRgEdRmkaNA/8AqNHI398eOvUrY9oMUdKC0ANHTT+58StDroyDff8A5zU6N/D9E1t15osqfS6rXaU33unJ+fX54m7RYgyQZo3h46HUdCN481S+pXP2kg3a4h3QlpWbHjEw32f1cLH1C6KipLVF5RzG5RlomsPobZJVrEmq1BHGSd8Z8nAqy/EydzD5kJgORJz1Sj6ip6rEfPI7cAP1XsVA5x1uV7grlLYsSSl2g9Vl0NCXcNFOYRs0+Q91hPMnRo6klbzs1gdMxwzyMleODSDGDyvucV7OpCku8yiNOdd4ijF2L2QLiJJG2aNWgjf1XT4IQwWAVED2gWAt0tZZC59Wq6j3OlQoRpRwgiIqi8IiIAiIgCIiAIiIAiIgCwqqqsst40UTVwleo8ZgVdeVHwSGWURl1r5jv1IaLm3M/wArJnpionEcJ7S2pa5puxzdCCrMbbEOe5MVUhDcoGVvwj5k8T1K1rEXk3sr4fXtFu2imA3CeEOd+YWKxnYvkIbUwdjc2EsZLob/AHgdW+NyuLXt7pZljP24+h2LevQTS/K/tELPSuO9RtXShbpV0wtcW5gjUWWuYhEsEKjZ2qc9Rp9dTi6sxg8dfG5UtU0+qzdm8G7aRznA5I/1edzfIanxC6lrObmlCTXiuhVfQoRoynVin0TWd+RCthPFv5VfEcYGZwfYanK0H91vBwNnJUy4IwtItvBC+g7VnxfZQ8fJs05lfSt/pzP6fVtHzWXT7RD+lSxjk6UulPoLBQ1RTFj3NO9pLf4PpZZFEyxXIq39fDWcPwR9PR+h2bSnjUn1bfuS8mITy6SSOy/A2zWflbv87rPw+XLa12q1QxXUxDh19wXIlcSbzJ5N7pUYR0KOF4bfwtifwjGJAACcw630/cLaKOvDrWPkd/lz+a0ykoC0XJAaN5cQ1o8zuWRLtDRwC8lVHcfZjcJH36ZdPUq+heT4JZORcW1Nvu/wb5HKCri1PZjaIVQe8RujaHdzNvcyw73Q3ufNbNFLddhrBysl5EReHoREQBERAEREAREQBUOjBVaIDFfRgrHdhwUki9yeYIk4aOSsVeDMkaWPaHNIIIKnV5Ze6mMI5GyOSildSzXMd700p3ZD/TJ+SVlPm3AnwBK6dimExVDckrA4cLjULXpNhmkZW1dUxnwMe0i3IFwJC5tzY9pPtKbSb4r3Ona36px0z5c/z8+++759/wCPfJKIIgHSO1PFsbOMklt3QbyVvWHYKyCJsTBo3e4+85x1c89SVsGE4BBTMyxMy3N3uJLpHu+J7jq4rJfSLXbUVRj4lF5dyuH/AIrgvc1p1IrTqVbE+k6Kw+lWrUYcHLts8IyPEwHd3PsOHxeXyUZT0vHzB6LrNbhgkYWOGhXOq3D30Ty10bnwk3GX/wBjPw829FgvLeU+/T4817nY+nX/AGceyqPbk/b8f8MjDoFm4zjcdJFnIzPPdij4ud16KMfj9Oxt2Oke7gwREOvyvdebO7PTVs4qKhpbGyxZGd1huHmsFrYzq1NVVNQXHx8C++u4wWE8y5L3ZZo9l6rELS1czmtOrItcoHJrdwWwYb7OqWMgkOeRzsAt2jpraAeAWTHTrvRaisRWF4HBk3N5k8/PnAw6KkbG0NY0NHIKRiCrjp1fbHZQyepHsZVa8AXqiehERAEREAREQBERAEREAREQBERAEREAREQHhCoMQREBQacLFq8MZILPaD80Re5PMEbDsfTNdm7JpO/Xd6KYjpQBYAAcgLBEXrbfEJJF5sIVwCyIonp6iIgCIiAIiIAiIgP/2Q==',
               },
             } )
          
           }
         
           thingToSay = thingToSay.replace( /Pioui/ig, "Piwi" )
           setTimeout( () => Tts.speak( thingToSay, {} ), 1000 )
           setResults( [] );
         
           lastResponse = intent.response
         }
         
       }
     
     } catch ( e )
     {
       console.error( e );
     }
   };
 

  const changedIcon = {
    name: 'code',
    type: 'MaterialIcons'
  }

  /**
   * Render Input Toolbar
   * @param {*} props 
   * @returns 
   */
   const renderInputToolbar = ( props ) => (
    <View {...props} containerStyle={{
      paddingTop: 6,
    }}>
      {begin === true &&
        <Button
          solid
          backgroundColor={"#AABBCC"}
          textColor="white"
          icon={changedIcon} style={{ width: "100%", }}
          disabled={true}
          textStyle={{ color: '#fff', fontSize: 22 }}
        rippleColor="white"
          text={"Bienvenue"} />}
      {end === false && begin === false &&
        <Button
          solid
          backgroundColor={isSuscribe === true ? "#AA55AE" : "#AA99EE"}
          textColor="white"
          icon={changedIcon} style={{ width: "100%", }}
          disabled={isSuscribe || begin}
          textStyle={{ color: '#fff', fontSize: 22 }}
          rippleColor="white"
          text={isSuscribe == false ? "Je veux réagir !" : "On vous écoutes..."}
          onPress={() => startRecognizing( end )}></Button>}
    </View>
  );
  
 
  /**
   * User effect
   */
  useEffect( () =>
  {

    if ( !Toast )
    {
      React.createRef();
    }
    Toast.hide()
      
    initTts();

  
    Voice.onSpeechResults = async ( e ) =>
    {
      await onSpeechResults( e )
    };
    
    // Voice.onSpeechEnd =  (e) => onSpeechResultsEnd(e)
    

    setMessages([
        {
          _id: 1,
        text: `Bonjour ${ nom} bienvenue chez Pioui, service SAV Express gratuit et multimarques. Votre demande concerne votre : 

  🔌 Fournisseur d'énergie ?

  🌎 Opérateur internet ?

  📱 Opérateur mobile ?

  ❓ Un autre prestataire
  `,
        createdAt: new Date(),
          user: {
            _id: 2,
            name: 'Pioui',
            avatar: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTExYSExESEhYWFhEQEBEQEBAQERYRFhYXFxYTFhYZHioiGRsnHhYWIzMjJystMDAwGCE2OzYvOiovMC0BCwsLDw4PGxERGC8nIScvLy8vMC8vLy8vLy8tLy8vLy8vLy8vLy0tLy8vLy8vLS8vLy8vLy8vLy8vLy8vLy8vL//AABEIANcA6gMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABQIDBAYHAQj/xAA9EAABAwIDBQUECQMEAwAAAAABAAIDBBEFEiEGMUFRYRMicYGRBzJSsRQjQmJykqHB0UPh8IKissIVM5P/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAgMEBQEG/8QAMBEAAgEDAgMHAwQDAQAAAAAAAAECAwQREiExQVETImGBkcHwBXHRMlKhsWLh8UL/2gAMAwEAAhEDEQA/AO4oiIAiIgCIiAIiIAiIgCIiAIiIAijcTxmKAd9wvwHH0Wj4t7Sy0kRRX6u0VsKM58EVTrQhxZ0pFxeT2nVV/cZ+Y/wsmj9q8gP1sNxzY4H9CFa7SouhSryn4nX0WpYDt5TVJDc2R/wu7p8r7/JbUx4IuDccws84Sg8SRohUjNZi8laIiiTCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiA8c6ywK7tXNPZ2B4Emx8v5Kyjrrw4K05r8187cnwdmc1+efN+yktmRe5yPaCofnc03uCQ6++/EeK12WnW6bcwgVLyOOUnxLWkrD2ZwT6RLY3DG96Rw5ch1P8AK68JJQ1M5dWLlPSiHwfZGap1AysG97rho6dT0CmJdgIWjWV7jzaxoH63XTI4WtaGNaGtaLNaNwCwa9oAJsfBrXPPo0ErI7iUntsaI28Et9zjmL7MPh70bs4Gu4teOtuPktg2F26kicIZ3F7NAHE3I8+IWwVbGvbdpuNeBBBG8EHUHoVzzaPD+zf2jBa57wHPmr4tVFpmZ6kHSeuB9CQTNe0Oabg6ghXVzP2Y7Rlw7B53atvy4j9/VdMXOq03Tlg6NGqqsNSCIirLQiIgCIiAIiIAiIgCIiAIiIAiIgCIiAKh+unqqibKlAUuKsRVDXe6QeoII5LTvapj5p6cRMcWvmzNu02dkHvW6m4HmVi7A3pMM7R+hJlcxvNznHKB0uLq5U2458ipzWrHmR+10wfUSEcHFv5QG/stv2UoOxp2aWdIBK/n3h3R5C3qVz2U53a8TYnxXQdscUfTUz3wgGUmOmpmnd20rxHHfoCb/wCla7nuwjH5sZaHenKXzcoxramngk7EmSaYi4p6aJ881jxc1vuDq4hRUu2kTNZ6atpG7u1npj2Q1sC58ZcGDxspfAsEjo4cje8931lRO/WSWU6vle7edb2HDQLKpMRY4uYW2FrkusQ4biCPPcse5rxki6nK9okYWua4BzXsIc1zSNHAjeLcVpW0EANwdxuCpmGmFFW/Roxlpqpk1RTRX0hniLTNHGPsxua8PtuBvZYWPM1K00ZGerFGlYFWOgma4Gxa6x8ivoXCasSxteOIB8ivnGuFpT5Fdj9mmI54A0nddv8ACsuoaoKXQy2c9FRw6m8IiLmnWCIiAIiIAiIgCIiAIiIAiIgCIiAIiICg7/D5rwo03F+d14/xsvc4PDTNtNjPptRTyulDI4g5skYBzuBcD3TubuUdtKTdrM8bIoxljiYSbNAtdxta+nkt1xOjc9jgx1nWOUO90nkSNR4ri+PYowSOhlEtPILj6wZ4r3se83UehWSpdXUJJRSS5c/n3NtpZW9fLqPpnl9s88GZJUMGmZviSLLb9sJjNh7KlgL+xkpa5zW6kiCRrpQPAB58lzNtO5+rbSD4onCQf7dfUKdwTG6ikaQ3WPUuZO09nrvte1v80V8b+VSOmrjK5rb1XuvQV/pdOnLVbt/Z7+j9n6nRtosZhjpmziRvYvDZBKDdrmEZmkW330sFBbEdrUuNVI0xx6tp43byCReR3oAPPxMVs/glXJAXwxUj6V8hkhw6ubI6FvOSCSxMTS65DSCN9rXWwSuxd7cjIaGhFrGbtpK2Rotvjjytbf8AEbK+M+7sYpww8Pka77TT9Jq6eCOV7PouSWeSI2cx1VNBDFHm4OIzutysru0B1KuYphTKanytL3vfU0cs08pzSyymphvI93yG4LBxubetFGOGZqryjRcTd9aegA/z1W9+yytyvcy/Ihc5mkzOLuZJ8uC2bYeqyTt6/t/hW2SzBo5iliqpeJ9AA3F/NVKzSuu0K8uNwO4nkIiIehERAEREAREQBERAEREAREQBWaqTK0nyCuE21Oij5JRNYMPd7xLugNrj9VKKyeN4LuGy5ox0LmnyKvlI4w0BoFgNAEQieXXFvbhRhk0UoFs4cHeNv7FdpWsbZ7JtrgwEsGS9s7HOHQixHM6dVVWTaTSy08+z/hmm0moVO88JrH48eJ84wte82jBJ5jQDxPBdO2C9n75C2eqzFmhAdcF/Rt9cvN3HhzG+YDsLTU9nEds8agyNb2bT92MaeZuVszlGFFvefp+S+vdxW1N5fXh6L3foG2AAAAAAAAFgANAAFjVMirkeorEJ9CtcY7nNbNY22qB2Dz8OSX/5va//AKrSdqK/QsG83v8Ah/up/aaZ2R4LS4FrmkDiCLH5rnUlQ52jrl/uuNjbTTPfda2v6LbTjjiY6sm9kUtNwDuuAVK4FLlmYfvAeuiiyr9K+xB5EFaUjBPqfSWBTZomn7o9dxUktY2Iqg+Fvn+outnXGqLEmdyk8wTCIigWBERAEREAREQBERAEREAVmoqGxtzPcGgcSofH9poaYG5Dn/CD81yjaTbWWYnWw4DgPBaKVvKe72RRUrqOy3ZuG1W2F7xxnQ6Bo95x4X/hbvhVN2cbW8Q1rT/pFvnc+a4FsiDU19PGbkGUPd+GMGQ/8V9E24KdwoxShEhb6pNzkeFUKtULKjQyleoUUjwpKtvKrKtPK9R4WJSoyrZdSEhWHOrIkGavilMDfRaHjuGWJc0a8Rz/ALrpddGtVxeLetVORmqRTOeFVxlZOJwZXXHHf4rDBWnJhkjqfsyxwNHZOOoItddVikDhcL5lw6tdG4OadR8l1bZPbFrgGvdY7tf36LJcUNT1xNdpcJLRI6Ois09S14u0g+avLnnTCIiAIiIAiIgCIiAK1PHmaW3Lbgi7TZw8CrqIDSK/2cQSkuM81zrqWO/ZR7vZJTnfUS/lYujore3qfuKuwp9DTtndgaejlbPG6RzwHNGctygOGpsBvW4FW3e80fiKuFQlJy3bJxio7IoKpVRVKBnhQovCvTwpcrLyrrisd5UkRLEhWFOVlylYM5ViIswahaxi43rZKl61vFSroFMjT8UjvdQTVsGIKBlHeK0oxzW5S1yy4Kgg3BIPMK2ymurjaF/BeqRRKOTZ8E2xngI72YciV0PBfaJBJZsl4zzOo9Vxf6HKPslVNjlH2HeijOnTqfqROnVrU/0vyZ9KUlbHILxva8fdIKyV83UlXURm7A9p5tutmwvaLFTYRmV/Qxh7fUhZpWX7Z+prjfv/AN035f7wdrRQ2zD6p0IdVhrZDua0AEN624qZWOUdLaz6HQhLVFPGPuERFEkEREAREQBERAY73d9o6O/ZXisOsdaWHqXj/asouUny+c2eLizwrwlCVSSh4wvChKpJUjwpeVYeVceVjyFSSImPMVgVD1lTvUVVSqyKK2YlXIoDEX6KTqpVAYhKr4oqkQNe5Qk3vKUrXqKf7ytRlqGZRzFu8Bw5G/zC6dsrskypp2VAcY8xeMh71sri3f5LmELF33YCAsoYQeIe78z3H91G5k4QyupGzip1WmuX4MKPYaMb3k+SyotjKcb8x81sqLnutPqdZUYLkRNPs9Ts3RN8SLn9VJRwtbuaB4BXEUXJviySjFcEERFEkEREAREQBERAEREBE48/I2OT4JGE/hJsVmyusreL0/aQyM4lpt4jUfJYGGVnawMfxtkf+Nvdd8r+asW8V4EM4kZgmVYkCj5JFT2690gki5UlywfpCGoXuDwyXvWLNIrb6hYU9QppEGymqmUPVSq9UzqLqZVbFFbMeqlUBXyqRq5lAVsytRVIj6p6wI9TdXql68p2KyKMlWWES+DUhkkawfaIHlxX0HhUAjijYBazWi3kuW+zbBy+TtCNG2aPE7z6LryzXs8tR6Gn6dTxF1Hz/oIiLCdIIiIAiIgCIiAIiIAiIgCIiALTIZvo1Y+mdpHUfXU5O7tNzmedv0HNbmtd2zwH6XAWsOWVh7SB97EPH2b8Abb+BseCspSSeJcH8z5f0QqJ4yuK+fPEpqXWWG6Za9gG1nbH6PUjsqhpMZzd0SOGhH3ZOY48OSkJ5bFX6HF4ZUqiksoz+3VDqlRZqlQape6RqJJ9SsWaoWE+pWLNVBSwQbL1ROoypnVuoq1FVVSppEGzysqFC1MqvVM6jZX3Nh5qxFM5YWWUe8VL4RQue4AC5JAA6rEoqUuK6xsBs1ltM9uv2ARuHNTclTjqZkUHXnpXn9ja9lsJEELW21A1PNx3lTipa2wsqlyZScm2zuwioxUUERFEkEREAREQBERAEREAREQBFbfMAsSavAXuAZ6pLxzUP9Pc42brztuA5k8Fg4hibGDvPLz8LDZvm7+FGTUVmTPYxcniKya/7StmYZwZ43MZOB3mkhrJGjmdweBuPHceFtFwvauRgEdRmkaNA/8AqNHI398eOvUrY9oMUdKC0ANHTT+58StDroyDff8A5zU6N/D9E1t15osqfS6rXaU33unJ+fX54m7RYgyQZo3h46HUdCN481S+pXP2kg3a4h3QlpWbHjEw32f1cLH1C6KipLVF5RzG5RlomsPobZJVrEmq1BHGSd8Z8nAqy/EydzD5kJgORJz1Sj6ip6rEfPI7cAP1XsVA5x1uV7grlLYsSSl2g9Vl0NCXcNFOYRs0+Q91hPMnRo6klbzs1gdMxwzyMleODSDGDyvucV7OpCku8yiNOdd4ijF2L2QLiJJG2aNWgjf1XT4IQwWAVED2gWAt0tZZC59Wq6j3OlQoRpRwgiIqi8IiIAiIgCIiAIiIAiIgCwqqqsst40UTVwleo8ZgVdeVHwSGWURl1r5jv1IaLm3M/wArJnpionEcJ7S2pa5puxzdCCrMbbEOe5MVUhDcoGVvwj5k8T1K1rEXk3sr4fXtFu2imA3CeEOd+YWKxnYvkIbUwdjc2EsZLob/AHgdW+NyuLXt7pZljP24+h2LevQTS/K/tELPSuO9RtXShbpV0wtcW5gjUWWuYhEsEKjZ2qc9Rp9dTi6sxg8dfG5UtU0+qzdm8G7aRznA5I/1edzfIanxC6lrObmlCTXiuhVfQoRoynVin0TWd+RCthPFv5VfEcYGZwfYanK0H91vBwNnJUy4IwtItvBC+g7VnxfZQ8fJs05lfSt/pzP6fVtHzWXT7RD+lSxjk6UulPoLBQ1RTFj3NO9pLf4PpZZFEyxXIq39fDWcPwR9PR+h2bSnjUn1bfuS8mITy6SSOy/A2zWflbv87rPw+XLa12q1QxXUxDh19wXIlcSbzJ5N7pUYR0KOF4bfwtifwjGJAACcw630/cLaKOvDrWPkd/lz+a0ykoC0XJAaN5cQ1o8zuWRLtDRwC8lVHcfZjcJH36ZdPUq+heT4JZORcW1Nvu/wb5HKCri1PZjaIVQe8RujaHdzNvcyw73Q3ufNbNFLddhrBysl5EReHoREQBERAEREAREQBUOjBVaIDFfRgrHdhwUki9yeYIk4aOSsVeDMkaWPaHNIIIKnV5Ze6mMI5GyOSildSzXMd700p3ZD/TJ+SVlPm3AnwBK6dimExVDckrA4cLjULXpNhmkZW1dUxnwMe0i3IFwJC5tzY9pPtKbSb4r3Ona36px0z5c/z8+++759/wCPfJKIIgHSO1PFsbOMklt3QbyVvWHYKyCJsTBo3e4+85x1c89SVsGE4BBTMyxMy3N3uJLpHu+J7jq4rJfSLXbUVRj4lF5dyuH/AIrgvc1p1IrTqVbE+k6Kw+lWrUYcHLts8IyPEwHd3PsOHxeXyUZT0vHzB6LrNbhgkYWOGhXOq3D30Ty10bnwk3GX/wBjPw829FgvLeU+/T4817nY+nX/AGceyqPbk/b8f8MjDoFm4zjcdJFnIzPPdij4ud16KMfj9Oxt2Oke7gwREOvyvdebO7PTVs4qKhpbGyxZGd1huHmsFrYzq1NVVNQXHx8C++u4wWE8y5L3ZZo9l6rELS1czmtOrItcoHJrdwWwYb7OqWMgkOeRzsAt2jpraAeAWTHTrvRaisRWF4HBk3N5k8/PnAw6KkbG0NY0NHIKRiCrjp1fbHZQyepHsZVa8AXqiehERAEREAREQBERAEREAREQBERAEREAREQHhCoMQREBQacLFq8MZILPaD80Re5PMEbDsfTNdm7JpO/Xd6KYjpQBYAAcgLBEXrbfEJJF5sIVwCyIonp6iIgCIiAIiIAiIgP/2Q==',
          },
        },
    ] )
    
    Tts.setDefaultLanguage('fr-FR');


    Tts.getInitStatus().then( ( res ) =>
    {
      const thingToSay = `Bonjour ${ nom } bienvenue chez Piwi, service S.A.V. express gratuit et multimarques. 
        Votre demande concerne votre fournisseur d'énergie ?  Un opérateur internet ? Un opérateur mobile ? ou un autre prestataire ?`
      Tts.speak( thingToSay, {
      } )
    
      console.log( "Gooo begin tts" )
      Tts.addEventListener( 'tts-finish', ( event ) => setBegin( false ) );
    } );
    return () => {
      Voice.destroy().then(Voice.removeAllListeners);
    };
  }, ['messages'] )
  
  /**
   * Logout
   */
   const logout = async () =>
   {
     await AsyncStorage.removeItem( 'user' );
     navigation.navigate( 'Login' )
   }
   
  
   const [visible, setVisible] = useState( false );
   const hideMenu = async ( elt ) =>
   {
     let url = ''
     switch ( elt )
     {
       case 'propos':
         url = 'https://pioui.com/application-pioui-sav-express/'
         var supported = await Linking.canOpenURL( url );
         if ( supported )
         {
           await Linking.openURL( url );
         } else
         {
           Alert.alert( `Don't know how to open this URL: ${ url }` );
         }

         break;
       case 'protection':
         url = 'https://pioui.com/politique-de-protection-des-donnees/'
         
         var supported = await Linking.canOpenURL( url );
         if ( supported )
         {
           await Linking.openURL( url );
         } else
         {
           Alert.alert( `Don't know how to open this URL: ${ url }` );
         }
         break;
       
       case 'juridique':
         url = 'https://pioui.com/conditions-juridiques/'
         
         var supported = await Linking.canOpenURL( url );
         if ( supported )
         {
           await Linking.openURL( url );
         } else
         {
           Alert.alert( `Don't know how to open this URL: ${ url }` );
         }
         break;
       
       case 'cookies':
         url = 'https://pioui.com/politique-des-cookies/'
         
         var supported = await Linking.canOpenURL( url );
         if ( supported )
         {
           await Linking.openURL( url );
         } else
         {
           Alert.alert( `Don't know how to open this URL: ${ url }` );
         }
         break;
       
       
     
       default:
         break;
     }
     setVisible( false )
   };
    const showMenu = () => setVisible(true);

  
    const goHome = async () =>
    {
        const value = await AsyncStorage.getItem('user');
        let currentStep = 0
        let sentence = ""
        let lastWord = ""
        let typeFournisseur = ''
        let nameFournisseur = ''
        let problemeType = ''
        let lastStep = 0
        let prix = 0
    
        await Tts.stop();
        await Voice.stop()
        let user = JSON.parse( value )
        console.log( "user" );
        console.log(user);
        const uid = await axios.post( `${ url }user/create-conversion`, {
          phone: user.phoneNumber, email: user.email, name: user.displayName, uid: user.uid
        } )
        console.log( "new user ref " );
        console.log( uid.data );
        currentUserRef = uid.data 
        setUserRef( uid.data )
      console.log(" ****  ****  Fin annoncé !!!! ****  **** ");
      // setEnd( false )
      navigation.reset(
      {
        index: 0,
        routes: [{ name: 'Home', params: {
        nom: user.displayName,
        phone: user.phoneNumber,
        email: user.email,
        userRef: uid.data,
        messageries: []
          }
        }],
      })
      
    }

    if ( end === true )
    {
      return ( <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Image
          style={{ width: 200, height: 150 }}
          source={require( './assets/logook.png' )}
        />
        <Text style={{ fontSize: 20, padding: 5, textAlign: 'center' }}> Très bien {nom}, un conseiller va vous rappeler. </Text>
        <Text style={{ fontSize: 20, padding: 5, textAlign: 'center' }}> Merci de préparer une facture de votre fournisseur ou opérateur.</Text>
        <Text style={{ fontSize: 20, padding: 5, textAlign: 'center' }}> À bientôt sur votre assistant Pioui.</Text>
        <Button
          onPress={() => goHome()}
          solid
          backgroundColor={"#222222"}
          textColor="white"
          style={{ width: "100%", margin: 10, padding: 10 }}
          textStyle={{ color: '#fff', fontSize: 22 }}
          rippleColor="white"
          text={"Relancer l'assistant Pioui"} />
      </View> )
    } else {
      return ( <View style={{ flex: 1 }}>
        <View style={{ padding: 20, paddingTop: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start' }}>
            
          <TouchableOpacity onPress={showMenu}>
            <Icon name="ios-menu" size={25} color="#888" />
            <Menu
              visible={visible}
              onRequestClose={hideMenu}
            >
              <MenuItem onPress={() => hideMenu( 'propos' )}>A propos de Pioui</MenuItem>
              <MenuItem onPress={() => hideMenu( 'protection' )}>Politique de protection des données</MenuItem>
              <MenuItem onPress={() => hideMenu( 'juridique' )}>Conditions juridiques du site</MenuItem>
              <MenuItem onPress={() => hideMenu( 'cookies' )}>Politique des Cookies</MenuItem>
            </Menu>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => logout()}>
            <Icon name="ios-log-in" size={25} color="#FF7560" />
          </TouchableOpacity>
        </View>
        <Toast ref={( ref ) => Toast.setRef( ref )} />
        <GiftedChat
          messages={messages}
          alignTop
          alwaysShowSend
          scrollToBottom
          renderInputToolbar={renderInputToolbar}
          showUserAvatar
          renderAvatarOnTop
          renderUsernameOnMessage
          messagesContainerStyle={{ backgroundColor: 'white' }}
        />
        {
          Platform.OS === 'android' && <KeyboardAvoidingView behavior="padding" />
        }
      </View> )
    }
}


 
  
const LoginScreen = ({ navigation }) => {

  const [email, setEmail] = useState( '' )
  const [password, setPassword] = useState( '' )
  const [permission, setPermission] = useState( false );

  const _retrieveData = async () => {
        try {
          const value = await AsyncStorage.getItem('user');
          if (value !== null) {
            let user = JSON.parse( value )
            if ( user )
            {
              
              console.log( user )
              console.log("laaa")
              const uid = await axios.post( `${ url }user/create-conversion`, {
                phone: user.phoneNumber, email: user.email, name: user.displayName, uid: user.uid, isNew: true
              } )
            
              
              console.log( uid.data.data )
              console.log(uid.data.data)
              return navigation.navigate( 'Home', {
                nom: user.displayName,
                phone: user.phoneNumber,
                email: user.email,
                userRef: uid.data,
                messageries: []
              } )
            }
            
          } else
          {
            console.log("No user almost...")
          }
        } catch (error) {
          // Error retrieving data
          console.log(error)
        }
  };
  
  
    
  useEffect( () =>
  {
    Toast.hide()
    _retrieveData()
  }, [] )
  
  
  const loginPress  = () =>
  {
    auth()
    .signInWithEmailAndPassword(email, password)
    .then( async (userCredential) => {
      console.log( 'User account created & signed in!' );
      const user = userCredential.user;
      console.log(user)
      // console.log(user.phoneNumber,user.email, user.displayName,user.uid )
      
      const uid = await axios.post( `${ url }user/create-conversion`, {
        phone: user.phoneNumber, email: user.email, name: user.displayName, uid: user.uid
      })
      
      navigation.navigate( 'Home', {
          nom: user.displayName,
          phone: user.phoneNumber,
          email: user.email,
        userRef: uid.data,  // Todo : User ref
          messageries: []
      } )
    
      try {
        await AsyncStorage.setItem(
          'user',
          JSON.stringify(user)
        );
      } catch (error) {
        // Error saving data
      }
    } )
      .catch( error =>
      {
        Toast.show( {
        type: 'error',
        text1: 'Mauvaise identification',
        text2: 'Erreur de votre email et/ou mot de passe'
      });
      if (error.code === 'auth/email-already-in-use') {
        console.log('That email address is already in use!');
      }

      if (error.code === 'auth/invalid-email') {
        console.log('That email address is invalid!');
      }

      console.error(error);
    });
    
  }
  
  const goSuscribe = () =>
  {
    navigation.navigate( 'Subscribe' )
  }
  
 return (permission === false ? <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
        <Image
        style={{ marginTop: 100, width: 150, height: 120 }}
        source={require('./assets/logook.png')}
        />
        <Reinput style={{ marginLeft: 20, marginRight: 20 }} onChangeText={setEmail} label='Votre email' keyboardType='email-address' />
        <Reinput style={{ marginLeft: 20, marginRight: 20 }} activeColor={password.length < 6  ? "red": "green"} onChangeText={setPassword} label='Votre mot de passe 6 caract.' secureTextEntry={true} password={true}  />
      <Button
        textColor="#fff" rippleColor="white" text="Vérifier"
        textStyle={{ fontSize: 20, color: 'white' }} style={{ width: 200, backgroundColor:"orange"  }}
        text="Se connecter" onPress={() => loginPress()} />
      
      <TouchableOpacity style={{marginTop: 20}} onPress={() => goSuscribe()}>
        <Text>Pas encore inscrit ?</Text>
      </TouchableOpacity>
        
      <Toast ref={(ref) => Toast.setRef(ref)} />
 </View> : <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
   <Text>
     Pour pouvoir répondre à vos demandes, l'application Pioui a besoin de récupérer vos informations (mail, numéro de téléphone) pour pouvoir vous rappeler.
   </Text></View> )
}

  
  /**
   * Subscribe View
   * @param {*} param0 
   * @returns 
   */
   const SubscribeScreen = ( { navigation } ) =>
   {
     const [nom, setNom] = useState( '' )
     const [phone, setPhone] = useState( '' )
     const [email, setEmail] = useState( '' )
     const [password, setPassword] = useState( '' )
     const [repassword, setRepassword] = useState( '' )
     const [cgu, setCgu] = useState(false)
 
     const [userRef, setUserRef] = useState( '' )
     const reg = /\S+@\S+\.\S+/i
     const regPhone = /^[0-9]{10}$/i
       //TODO: Enlever cette exemple...
       //  navigation.navigate( 'Home', {
       //    nom: "Juju",
       //    phone: "0674585648"
       //  } )
       // return (<></>)
      
       // If null, no SMS has been sent
       const [confirm, setConfirm] = useState( false );
       const [code, setCode] = useState( '' );
 
       // Handle the button press
     const signInWithPhoneNumber = async ( phoneNumber ) =>
     {
       try
       {
         const response = await axios.post( `${ url }user/register-account`, {
           name: nom,
           phone: phone,
           email: email,
           password: password
         } )
         
         const confirmation = await axios.post( `${ url }user/phone-account`, {
            phone: phone,
           } )
         if ( confirmation.data === "pending" )
         {
           setConfirm( confirmation.data );
           setUserRef( response.data.data )
           
         }
       
         
       } catch ( e )
       {
         console.log( e )
       }
     }
 
     const confirmCode = async () =>
     {
       try
       {
         const confirmation = await axios.post( `${ url }user/verify-account`, {
           phone: phone,
           code
         } )
         if ( confirmation.data === "approved" )
         {
         await AsyncStorage.setItem(
           'user',
           JSON.stringify(
             {
               nom: nom,
               phone: phone,
               email: email,
               userRef: userRef
             }
           )
         );
           
         navigation.navigate( 'Home', {
           nom: nom,
           phone: phone,
           email: email,
           userRef: userRef,
           messageries: []
         } )
         } else
         {
           Alert.alert(`Vous avez saisis le mauvais code reçu par SMS`);
         }
         
       } catch ( error )
       {
         console.log( error )
         console.log( "noooo" )
         console.log( 'Invalid code.' );
       }
     }
       
 
     let cond =  cgu === false || nom.length < 3 || !reg.test(email) || password.length < 6  || password !== repassword || !regPhone.test(phone)
     
     return (
       <ScrollView>
       <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
         <Image
         style={{ width: 150, height: 80 }}
         source={require('./assets/logo.png')}
         />
         <Reinput style={{ marginLeft: 20, marginRight: 20 }} onChangeText={setNom} label='Votre nom' />
         <Reinput style={{ marginLeft: 20, marginRight: 20 }} onChangeText={setEmail} label='Votre email' keyboardType='email-address' />
         <Reinput style={{ marginLeft: 20, marginRight: 20 }} activeColor={password != repassword ? "red": "green"} onChangeText={setPassword} label='Votre mot de passe 6 caract.' secureTextEntry={true} password={true}  />
         <Reinput style={{ marginLeft: 20, marginRight: 20 }} activeColor={password != repassword ? "red": "green"} onChangeText={setRepassword} label='Re-saississez votre mot de passe' secureTextEntry={true} password={true}  />
         <Reinput style={{ marginLeft: 20, marginRight: 20 }} onChangeText={setPhone} label='Votre téléphone' keyboardType='numeric' maxLength={10} />
         <View>
           
           <View style={{margin: 10}}>
             <Text>
             L’application et la consultation du service Pioui et entièrement gratuite. J’ai bien pris note que les informations collectées par Pioui ne sont ni vendues ni commercialisées à quiconque. J’accepte d’être recontacté par Pioui pour répondre à ma et ou mes future(s) demande(s), Pioui pourra me recontacter pour la gestion futur de mes contrats. 
             Pour continuer, veuillez accepter nos conditions juridiques à l’adresse suivante : 
             </Text>
           </View>
              <View style={{margin: 15}}>
               <Text style={{color: 'blue'}}
                 onPress={ async () =>
               {
                 let url = 'https://pioui.com/politique-de-confidentialite/'
                 let supported = await Linking.canOpenURL( url );
                 if (supported) {
                     await Linking.openURL(url);
                 } else {
                     Alert.alert(`Don't know how to open this URL: ${url}`);
                 }
               }}>
                 Voir les Conditions Générales d'Utilisations
                 </Text>
             </View>
             
             <CheckBox
             title="J'accepte les conditions générales d'utilisation"
             checked={cgu}
             onPress={() => setCgu(!cgu)}  
           />
         </View>
        {confirm === false
             ? <Button
             textColor="white"
             rippleColor="white"
             text="Confirmer via SMS"
             textStyle={{ fontSize: 14 }}
             style={{backgroundColor:"orange", color: "white"}}
             backgroundColor={cond === false ? "green" : "red"}
             disabled={cond}
             onPress={() => signInWithPhoneNumber( '+33' + phone.slice( -9 ) )}
             />
         : <View>
               <Reinput value={code} onChangeText={text => setCode( text )} style={{ width: 250, marginLeft: 20, marginRight: 20 }} label='Code de confirmation' />
               <Button
                 text="Je m'inscris"
                 textColor="#fff" rippleColor="white" 
                 textStyle={{ fontSize: 20, color: 'white' }} style={{ width: 200, backgroundColor:"orange"  }}
                  onPress={() => confirmCode()} />
           </View>}
         </View>
       </ScrollView>);
   }
 
 
const Stack = createStackNavigator();


return (
  <NavigationContainer>
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen options={{ title: 'Discutons...', headerShown: false }} name="Home" component={HomeScreen} />
      <Stack.Screen options={{ title: 'Login...', headerShown: true }} name="Login" component={LoginScreen} />
      <Stack.Screen  options={{ title: 'Inscrivez-vous', headerShown: true }}  name="Subscribe" component={SubscribeScreen} />
    </Stack.Navigator>
  </NavigationContainer>
);
}


const styles = StyleSheet.create({
  container: {
    //  paddingTop: 100,
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
  },
   navBar: {
      height: 54,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderBottomWidth: 0,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 4,
      elevation: 1,
    },
    leftContainer: {
      justifyContent: 'flex-start',   
      flexDirection: 'row'
    },
    middleContainer: {
        flex: 2,
        backgroundColor: 'white',
        flexDirection: 'row',
        fontSize:18,
        marginLeft: 10,
        marginRight:10
      },
    rightContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    rightIcon: {
      paddingHorizontal:20,
      resizeMode: 'contain',
      backgroundColor: 'white',
    }
// titleTxt: {
//   marginTop: 100,
//   color: 'white',
//   fontSize: 28,
// },
// viewRecorder: {
//   marginTop: 40,
//   width: '100%',
//   alignItems: 'center',
// },
// recordBtnWrapper: {
//   flexDirection: 'row',
// },
// viewPlayer: {
//   marginTop: 60,
//   alignSelf: 'stretch',
//   alignItems: 'center',
// },
// viewBarWrapper: {
//   marginTop: 28,
//   marginHorizontal: 28,
//   alignSelf: 'stretch',
// },
// viewBar: {
//   backgroundColor: '#ccc',
//   height: 4,
//   alignSelf: 'stretch',
// },
// viewBarPlay: {
//   backgroundColor: 'white',
//   height: 4,
//   width: 0,
// },
// playStatusTxt: {
//   marginTop: 8,
//   color: '#ccc',
// },
// playBtnWrapper: {
//   flexDirection: 'row',
//   marginTop: 40,
// },
// btn: {
//   borderColor: 'white',
//   borderWidth: 1,
// },
// txt: {
//   color: 'white',
//   fontSize: 14,
//   marginHorizontal: 8,
//   marginVertical: 4,
// },
// txtRecordCounter: {
//   marginTop: 32,
//   color: 'white',
//   fontSize: 20,
//   textAlignVertical: 'center',
//   fontWeight: '200',
//   fontFamily: 'Helvetica Neue',
//   letterSpacing: 3,
// },
// txtCounter: {
//   marginTop: 12,
//   color: 'white',
//   fontSize: 20,
//   textAlignVertical: 'center',
//   fontWeight: '200',
//   fontFamily: 'Helvetica Neue',
//   letterSpacing: 3,
// },
});

export default gestureHandlerRootHOC(App);